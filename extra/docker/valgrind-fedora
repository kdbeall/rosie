FROM fedora
ARG branch

RUN dnf -y update && dnf install -y \
  gcc \
  git \
  make \
  readline-devel \
  valgrind \
  libbsd \
  libbsd-devel

# Must symlink or copy these test files into the docker directory
COPY syslog100k /opt/data/
COPY syslog-2018-09-05.rpl /opt/data/

# This COPY is designed to trigger re-running the git clone when the repo changes.
COPY githead-$branch /opt/githead-$branch

WORKDIR /opt
RUN git clone --recursive --depth 1 --branch $branch https://gitlab.com/rosie-pattern-language/rosie.git /opt/rosie

WORKDIR /opt/rosie
RUN make LUADEBUG=1
RUN make test
RUN make install

# Here is one simple invocation of valgrind, just to make sure it
# works.  Get an interactive shell to this container to run more tests.
RUN valgrind --leak-check=full rosie -f /opt/data/syslog-2018-09-05.rpl match -o byte syslog /opt/data/syslog100k >/dev/null



RUN uname -a
RUN cat /etc/fedora-release
