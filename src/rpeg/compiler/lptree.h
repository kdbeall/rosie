/*  
** $Id: lptree.h,v 1.2 2013/03/24 13:51:12 roberto Exp $
*/

#if !defined(lptree_h)
#define lptree_h


#include "config.h"
#include "lptypes.h" 
#include "ktable.h"

/* number of siblings for each tree */
static const byte numsiblings[] = {
  0, 0, 0,	/* char, set, any */
  0, 0,		/* true, false */	
  1,		/* rep */
  2, 2,		/* seq, choice */
  1, 1,		/* not, and */
  0, 0, 2, 1,  /* call, opencall, rule, grammar */
  1,	       /* behind */
  1, 1,	       /* capture, runtime capture */
  0,	       /* Rosie backreference */
  0	       /* halt (rosie) */
};

/*
** types of trees
*/
typedef enum TTag {
  TChar = 0, TSet, TAny,  /* standard PEG elements */
  TTrue, TFalse,
  TRep,
  TSeq, TChoice,
  TNot, TAnd,
  TCall,
  TOpenCall,
  TRule,     /* sib1 is rule's pattern, sib2 is 'next' rule */
  TGrammar,  /* sib1 is initial (and first) rule */
  TBehind,   /* match behind */
  TCapture,  /* regular capture */
  TRunTime,  /* run-time capture */
  TBackref,  /* Rosie: match previously captured text */
  THalt,     /* rosie */
} TTag;

/*
** Tree trees
** The first sibling of a tree (if there is one) is immediately after
** the tree.  A reference to a second sibling (ps) is its position
** relative to the position of the tree itself.  A key in ktable
** uses the (unique) address of the original tree that created that
** entry. NULL means no data.
*/
typedef struct TTree {
  byte tag;
  byte cap;		/* kind of capture (if it is a capture) */
  int32_t key;		/* key in ktable for capture name (0 if no key) */
  union {
    int ps;		/* occasional second sibling */
    int n;		/* occasional counter */
  } u;
} TTree;


/*
** A complete pattern has its tree plus, if already compiled,
** its corresponding code
*/
typedef struct Pattern {
  union Instruction *code;
  int codesize;
  Ktable *kt;
  TTree tree[1];		/* tree must be last, because it will grow */
} Pattern;


/* number of siblings for each tree */
/* extern const byte numsiblings[]; */

/* access to siblings */
#define sib1(t)         ((t) + 1)
#define sib2(t)         ((t) + (t)->u.ps)


#endif

