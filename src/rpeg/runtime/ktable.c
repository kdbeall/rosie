/*  -*- Mode: C; -*-                                                         */
/*                                                                           */
/*  ktable.c                                                                 */
/*                                                                           */
/*  © Copyright Jamie A. Jennings 2018.                                      */
/*  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)  */
/*  AUTHOR: Jamie A. Jennings                                                */

/* TODO: ktable holds constant captures (Crosieconst) but we want
   those to be unicode, and ktable cannot hold unicode because it is
   implemented using null-terminated strings.  In future, even rosie
   pattern names could be unicode, and that is what ktable was meant
   for.  So we should change the impl to store string length, too.
*/


/*
 * Capture table
 *
 * In lpeg, this is a Lua table, used as an array, with values of any
 * type.  In Rosie, the value type is always string.  In order to have
 * a Rosie Pattern Matching engine that can be independent of Lua, we
 * provide here a stand-alone ktable implementation.
 * 
 * Operations 
 *
 * new, free, element, len, concat
 *
 */

/*
 * 'block' holds consecutive null-terminated strings;
 * 'block[elements[i]]' is the first char of the element i;
 * 'blocknext' is the offset into block of the first free (unused) character;
 * 'element[next]' is the first open slot, iff len <= size;
 * 'size' is the number of slots in the element array, size > 0;
 *
 *  NOTE: indexes into Ktable are 1-based
 */

#if defined(__GNUC__)
#define _GNU_SOURCE
#endif

#if defined(__linux__)
#define qsort_r_common(base, n, sz, context, compare) qsort_r((base), (n), (sz), (compare), (context))
#else
#define qsort_r_common qsort_r
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "config.h"
#include "ktable.h"

#if !defined(KTABLE_DEBUG)
#define KTABLE_DEBUG 0
#endif 

#define LOG(msg) \
     do { if (KTABLE_DEBUG) fprintf(stderr, "%s:%d:%s(): %s", __FILE__, \
			       __LINE__, __func__, msg); \
	  fflush(stderr); } while (0)

#define LOGf(fmt, ...) \
     do { if (KTABLE_DEBUG) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
			       __LINE__, __func__, __VA_ARGS__); \
	  fflush(stderr); } while (0)

Ktable *ktable_new(int initial_size, size_t initial_blocksize) {
  Ktable *kt = malloc(sizeof(Ktable));
  if (!kt) return NULL;
  kt->size = 1 + ((initial_size > 0) ? initial_size : KTABLE_INIT_SIZE);
  kt->elements = calloc(kt->size, sizeof(int));
  if (!kt->elements) return NULL;
  kt->next = 1;
  kt->blocksize = ((initial_blocksize > 0) ? ((size_t) initial_blocksize) : ((size_t) kt->size * KTABLE_AVG_ELEMENT_LEN));
  kt->block = (char *) malloc(kt->blocksize);
  if (!kt->block) return NULL;
  kt->blocknext = 0;
  return kt;
}

void ktable_free(Ktable *kt) {
  LOG("in ktable_free\n");
  if (kt) {
    free(kt->block);
    free(kt->elements);
    free(kt);
  }
}

int ktable_len (Ktable *kt) {
  if (!kt) {
    LOG("null kt\n");
    return 0;
  }
  return kt->next - 1;
}

/* 1-based indexing */
const char *ktable_element (Ktable *kt, int i) {
  if (!kt) {
    LOG("null kt\n");
    return NULL;
  }
  return (((i) < 1) || ((i) >= (kt)->next)) ? NULL : & ((kt)->block[ (kt)->elements[(i)] ] );
}

/* 
 * Concatentate the contents of table 'kt1' into table 'kt2'.
 * Return the original length of table 'kt2' (or 0, if no
 * element was added, as there is no need to correct any index).
 */

int ktable_concat(Ktable *kt1, Ktable *kt2, int *ret_n2) {
  if (!kt1 || !kt2) {
    if (!kt1) LOG("null k1\n");
    else {
      LOG("null k2\n");
    }
    return KTABLE_ERR_NULL;
  }
  int n1 = ktable_len(kt1);
  assert( n1 >= 0 );
  int n2 = ktable_len(kt2);
  assert( n2 >= 0 );
  LOGf("kt1 len %d, kt2 len %d\n", n1, n2);
  int lastidx;
  if ((n1 + n2) > KTABLE_MAX_SIZE) return KTABLE_ERR_SIZE;
  if (n1 == 0) return 0;  /* kt1 is empty, and no indexes to correct */
  for (int i = 1; i <= n1; i++) {
    lastidx = ktable_add(kt2, ktable_element(kt1, i));
    if (lastidx < 0) return KTABLE_ERR_MEM;
    assert( lastidx == (n2 + i) );
  }
  assert( ktable_len(kt2) == (n1 + n2) );
  assert( n2 >= 0 );
  *ret_n2 = n2;
  return KTABLE_OK;
}
  
static int extend_ktable_elements(Ktable *kt) {
  if (!kt) {
    LOG("null kt\n");
    return KTABLE_ERR_NULL;
  }
  size_t newsize = 2 * sizeof(int) * kt->size;
  void *temp = realloc(kt->elements, newsize);
  if (!temp) return KTABLE_ERR_MEM;
  kt->elements = temp;
  kt->size = 2 * kt->size;
  LOGf("extending element array to %d slots (%d usable)\n", kt->size, kt->size - 1);
  return KTABLE_OK;
}

static int extend_ktable_block(Ktable *kt, size_t add_at_least) {
  if (!kt) {
    LOG("null kt\n");
    return KTABLE_ERR_NULL;
  }
  size_t newsize = 2 * kt->blocksize;
  if (add_at_least > kt->blocksize) newsize += add_at_least;
  void *temp = realloc(kt->block, newsize);
  if (!temp) return KTABLE_ERR_MEM;
  kt->block = temp;
  kt->blocksize = newsize;
  LOGf("extending block to %zu bytes\n", kt->blocksize);
  return KTABLE_OK;
}

/* 
 * Return index of new element (1-based).  If element is too long, it
 * will be truncated.  Length limit should be checked before we get here.
 */
int ktable_add(Ktable *kt, const char *element) {
  if (!kt) {
    LOG("null kt\n");
    return KTABLE_ERR_NULL;
  }
  assert( element != NULL );
  assert( kt->next > 0 );

  size_t len = elementlen(element);
  if (KTABLE_DEBUG)
    if (len == 0)
      LOG("WARNING! element length is zero\n");

  if (kt->next == kt->size)
    if (extend_ktable_elements(kt) < 0) return KTABLE_ERR_MEM;
  if (blockspace(kt) < (len + 1))
    if (extend_ktable_block(kt, len + 1) < 0) return KTABLE_ERR_MEM;
  assert( kt->next < kt->size );
  assert( blockspace(kt) > len );

  strncpy(kt->block + kt->blocknext, element, len);
  
  kt->elements[kt->next] = kt->blocknext;
  kt->blocknext += len;
  kt->block[kt->blocknext - 1] = '\0';

  kt->next++;
  assert( kt->next > 0 );

  LOGf("new element '%s' added in position %d/%d\n", element, kt->next - 1, kt->size - 1);
  return kt->next - 1;
}

#if defined(__linux__)
static int ktable_entry_cmp(const void *s1, const void *s2, void *parm)
#else
static int ktable_entry_cmp(void *parm, const void *s1, const void *s2)
#endif
{
  Ktable *kt = (Ktable *) parm;
  const int index1 = *((const int *) s1);
  const int index2 = *((const int *) s2);
  return strncmp(&kt->block[index1], &kt->block[index2], KTABLE_MAX_ELEMENT_LEN); 
}

/* Create a new index to an existing ktable, where the elements are in
   sorted order.  The new index will need to be freed by the caller.
*/
int *ktable_sorted_index(Ktable *kt) {
  size_t sz = ktable_len(kt) * sizeof(int);
  int *elements = (int *)malloc(sz);
  memcpy(&elements[0], &kt->elements[1], sz); /* Ktable indexing is 1-based */ 
  qsort_r_common(&elements[0], (size_t) ktable_len(kt), sizeof(int), (void *)kt, &ktable_entry_cmp);
  return elements;
}

/* Create a new ktable from orig, where the new one has no duplicates
   and whose elements are in sorted order.  The orig table is not
   modified or freed.
*/
Ktable *ktable_compact(Ktable *orig) {
  assert( orig != NULL );
  int orig_len = ktable_len(orig);
  if (orig_len == 0) return orig;
  int *index = ktable_sorted_index(orig);
  Ktable *new = ktable_new(orig_len, orig->blocksize);
  ktable_add(new, &orig->block[index[0]]);
  for (int i = 1; i < orig_len; i++)
    if (strncmp(&orig->block[index[i-1]], &orig->block[index[i]], KTABLE_MAX_ELEMENT_LEN) != 0) 
      ktable_add(new, &orig->block[index[i]]);
  free(index);
  return new;
}

/* Given a COMPACT, SORTED ktable, search for an element matching
   'target', returning its index or 0 (if not found).
*/
int ktable_compact_search(Ktable *kt, const char *target) {
  // bsearch does not have a bsearch_r variant in C99?  Argh.
  // int *result = bsearch(target, &kt->elements[0], (size_t) ktable_len(kt), sizeof(int), &ktable_entry_cmp);
  int len = ktable_len(kt);
  if (len == 0) return 0;
  int i = 1;
  int j = len;
  int mid, test;
  while (1) {
    mid = i + ((j - i) / 2);
    test = strncmp(target, ktable_element(kt, mid), KTABLE_MAX_ELEMENT_LEN);
    if (test == 0) return mid;
    if (i == j) return 0;	/* not found */
    if (test < 0) j = mid - 1;
    else { i = mid + 1; }
  }
}

void ktable_dump(Ktable *kt) {
  if (!kt) printf("Ktable pointer is NULL\n");
  else {
    printf("Ktable: size = %d (%d used), blocksize = %lu (%lu used)\n",
	   (kt->size - 1), (kt->next - 1), kt->blocksize, kt->blocksize - blockspace(kt));
    printf("contents: ");
    for (int i = 1; i < kt->next; i++) printf("%d: %s ", i, ktable_element(kt, i));
    printf("\n");
  }
  fflush(stdout);
}


