-- -*- Mode: Lua; -*-                                                                             
--
-- cli-test-command.lua    Implements the 'test' command of the cli
--
-- © Copyright IBM Corporation 2017.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHORS: Jamie A. Jennings, Kevin Zander

-- NOTE: This code badly needs to be refactored.

local rosie = import("rosie")

local p = {}
local cli_common = import("cli-common")
local io = import("io")
local common = import("common")
local ustring = import("ustring")
local violation = import("violation")

local write_error = function(...) io.stderr:write(...) end

local function startswith(str,sub)
  return string.sub(str,1,string.len(sub))==sub
end

local split = util.split

local function find_test_lines(str)
  local num = 0
  local lines = {}
  for _,line in pairs(split(str, "\n")) do
     if startswith(line,'-- test') or startswith(line,'--test') then
        table.insert(lines, line)
        num = num + 1
     end
  end
  return num, lines
end

-- setup the engine that will parse the test lines in the rpl file
function p.setup(en)
   local test_patterns =
      [==[
	 includesKeyword = ("includes" / "excludes")
	 includesClause = includesKeyword rpl_1_2.identifier
	 testKeyword = "accepts" / "rejects"
	 test_local = "local"
	 test_line = ("--test"/"-- test")
	             test_local?
	             rpl_1_2.identifier 
	             (testKeyword / includesClause) 
		     (rpl_1_2.quoted_string ("," rpl_1_2.quoted_string)*)?
   ]==]
   local libdir = rosie.attributes.ROSIE_LIBDIR
   local ok, errs = en:loadfile(libdir .. "/rosie/rpl_1_2.rpl")
   if not ok then
      for _,v in ipairs(errs or {}) do
	 write_error(violation.tostring(v))
      end
      error("Internal error")
   end
   local ok = en:load(test_patterns)
   assert(ok, "Internal error: test_patterns failed to load")
end   

local function shorten(str, len)
   if #str > len then
      return "..." .. str:sub(#str-len+4)
   end
   return str
end

local function indent(str, col)
   return str:gsub('\n', '\n' .. string.rep(" ", col))
end

function p.run(rosie, en, args, filename)
   local right_column = 4
   -- fresh engine for testing this file
   local test_engine = rosie.engine.new()
   -- set it up using whatever rpl strings or files were given on the command line
   cli_common.setup_engine(rosie, test_engine, args)
   io.stdout:write(filename, '\n')
   -- load the rpl code we are going to test (second arg true means "do not search")
   local ok, pkgname, errs, actual_path = test_engine:loadfile(filename, true)
   if not ok then
      local msgs = list.map(violation.tostring, errs)
      local msg = table.concat(msgs, "\n")
      io.write(indent(msg, right_column), "\n")
      return false, 0, 0
   end
   local function write_test_result(...)
      io.write(string.rep(" ", right_column))
      for _,item in ipairs{...} do io.write(item); end
      io.write("\n")
   end
   if args.verbose then
      write_test_result("File compiled successfully")
   end
   -- read the tests out of the file and run each one
   local f, msg = io.open(filename, 'r')
   if not f then error(msg); end
   local num_patterns, test_lines = find_test_lines(f:read('*a'))
   f:close()
   if num_patterns == 0 then
      write_test_result("No tests found")
      return true, 0, 0
   end
   local function find_local(en, pkgname, identifier)
      -- returns an rplx if identifier found in pkgname, or
      -- false if not found, or
      -- nil if found but is exported, i.e. not a local 
      local pkgenv = en.env:lookup(pkgname)
      if pkgenv then
	 local pat = pkgenv:lookup(identifier)
	 if pat then
	    if pat.exported then
	       return nil
	    end
	    return rosie.rplx.new(en, pat)
	 end
      end
      return false
   end
   local function write_compilation_error(exp, message)
      write_test_result("ERROR: " .. message .. ": ",
			tostring(exp))
   end
   local function test_exp(original_exp, q, should_accept, is_local)
      -- returns status, result
      -- where status is true if test passed, false if test failed, nil if error
      local exp = original_exp
      if pkgname then
	 exp = common.compose_id({pkgname, original_exp})
      end
      local ok, res, leftover
      if not is_local then
	 -- The binding we want to test is not local, so it must be visible in
	 -- test_engine. 
	 ok, res, leftover = test_engine:match(exp, q)
	 if (not ok) then
	    local found_as_local = find_local(test_engine, pkgname, original_exp)
	    if found_as_local then
	       write_compilation_error(original_exp, "pattern is local; use 'test local' instead")
	    else
	       write_compilation_error(original_exp, "pattern did not compile (or is not defined)")
	    end
	    return nil
	 end
      else
	 -- The binding we want to test is local to the package, so it is not
	 -- visible in the environment of test_engine.  But we can extract it to
	 -- run the tests.
	 local compiled_pattern = find_local(test_engine, pkgname, original_exp)
	 if not compiled_pattern then
	    -- User wants to test a local, but it's either not there or is exported
	    if compiled_pattern == nil then
	       write_compilation_error(original_exp, "pattern is exported; use 'test' not 'test local'")
	    else
	       write_compilation_error(original_exp, "pattern did not compile (or is not defined)")
	    end
	    return nil
	 end -- if not compiled_pattern
	 assert(rosie.rplx.is(compiled_pattern))
	 ok = true
	 res, leftover = compiled_pattern:match(q)
      end -- if is_local
      -- At this point, ok is true, and useful values are in: res, leftover
      local accepted = res and (leftover == 0)
      return (should_accept == accepted), res
   end
   -- return values: true, false, nil (where nil means failure to match), or "error"
   local function test_includes_ident(exp, q, id, is_local)
      local function searchForID(tbl, id)
	 if not tbl then return false; end
         -- tbl MUST BE "subs" table from a match
         local found = false
         for i = 1, #tbl do
            if tbl[i].subs ~= nil then
               found = searchForID(tbl[i].subs, id)
               if found then break end
            end
            if tbl[i].type == id then
               found = true
               break
            end
         end
         return found
      end
      local matched, res = test_exp(exp, q, true, is_local)
      if matched == nil then
	 return "error"
      elseif matched == false then
	 return nil -- failure to match
      end
      assert(res)
      return searchForID(res.subs, id)
   end
   local failures, errors, blocked, passed, total = 0, 0, 0, 0, 0
   local test_rplx, errs = en:compile("test_line")
   if errs then
      errs = table.concat(map(violation.tostring, errs), "\n");
      assert(test_rplx, "internal error: test_line failed to compile: " .. errs)
   end
   for _,p in pairs(test_lines) do
      local m, left = test_rplx:match(p)
      -- literals to test against will be in subs at offset 3
      local literals = 3
      if not m then
	 write_test_result("ERROR: invalid test syntax: ", p)
	 total = total + 1
	 errors = errors + 1
      else
	 local is_local_identifier = (m.subs[1].type=="test_local")
	 if is_local_identifier then
	    table.remove(m.subs, 1)
	 end
	 if #m.subs < literals then
	    write_test_result("ERROR: invalid test syntax (missing quoted input strings): ", p)
	    total = total + 1
	    errors = errors + 1
	 else
	    local testIdentifier = m.subs[1].data
	    local testType = m.subs[2].type
	    if testType == "includesClause" then
	       -- test includes
	       local t = m.subs[2]
	       assert(t.subs and t.subs[1] and t.subs[1].type=="includesKeyword")
	       local testing_excludes = (t.subs[1].data=="excludes")
	       assert(t.subs[2] and t.subs[2].type=="identifier",
		      "not an identifier: " .. tostring(t.subs[2].type))
	       local containedIdentifier = t.subs[2].data
	       for i = literals, #m.subs do
		  total = total + 1
		  local includes, teststr, esc_err, msg
		  teststr, esc_err = ustring.unescape_string(m.subs[i].data)
		  if not teststr then
		     msg = esc_err or "test input is not a valid Rosie string"
		     includes = "error"
		  else
		     includes = test_includes_ident(testIdentifier,
						    teststr,
						    containedIdentifier,
						    is_local_identifier)
		     if includes==nil then
			msg = "cannot test if " .. testIdentifier ..
			   " includes/excludes " .. containedIdentifier ..
			   " because " .. testIdentifier .. " did not accept " .. ustring.requote(teststr)
		     elseif includes=="error" then
			includes = nil
			msg = "cannot test if " .. testIdentifier ..
			   " includes/excludes " .. containedIdentifier --.. " because " .. testIdentifier .. " did not compile"
		     elseif (not testing_excludes and not includes) then
			msg = testIdentifier .. " did not include " .. containedIdentifier ..
			   " with input " .. ustring.requote(teststr)
		     elseif (testing_excludes and includes) then
			msg = testIdentifier .. " did not exclude " .. containedIdentifier ..
			   " with input " .. ustring.requote(teststr)
		     else
			msg = nil
		     end
		  end -- if teststr is a valid Rosie string
		  if msg then
		     if includes==nil then
			blocked = blocked + 1
			write_test_result("BLOCKED: ", msg)
		     elseif includes=="error" then
			errors = errors + 1
			write_test_result("ERROR: ", msg)
		     else
			failures = failures + 1
			write_test_result("FAILED: ", msg)
		     end
		  else
		     passed = passed + 1
		  end
	       end -- for each literal
	    elseif testType == "testKeyword" then
	       -- test accepts/rejects
	       local should_accept = (m.subs[2].data == "accepts")
	       for i = literals, #m.subs do
		  total = total + 1
		  local teststr, esc_err
		  teststr, esc_err = ustring.unescape_string(m.subs[i].data) -- allow, e.g. \" inside the test string
		  if not teststr then
		     errors = errors + 1
		     write_test_result("ERROR: ", esc_err)
		  else
		     local result = test_exp(testIdentifier, teststr, should_accept, is_local_identifier)
		     if not result then
			if result==nil then
			   errors = errors + 1
			else
			   failures = failures + 1
			end
			if #teststr==0 then teststr = "the empty string"; end -- for display purposes
			if result ~= nil then
			   write_test_result("FAILED: ", testIdentifier, " did not ", m.subs[2].data:sub(1,-2), " ", m.subs[i].data)
			end
		     else
			passed = passed + 1
		     end -- if result
		  end -- if teststr is a valid Rosie string
	       end -- for
	    else -- unknown test type
	       assert(false, "parser for test expressions produced unexpected test type: " .. tostring(testType))
	    end
	 end -- if not m
      end -- for each test line
   end
   if failures == 0 and errors == 0 and blocked == 0 then
      write_test_result("All ", tostring(total), " tests passed")
   else
      write_test_result("Tests: ", tostring(total),
			"  Errors: ", tostring(errors),
			"  Failures: ", tostring(failures),
			"  Blocked: ", tostring(blocked),
			"  Passed: ", tostring(passed))
   end
   return true, failures, total
end

return p
